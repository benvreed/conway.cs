using System;

namespace GameOfLife
{
    class Game
    {
        /// <summary>
        /// The size.
        /// </summary>
        private static int size = 0;

        /// <summary>
        /// The grid.
        /// </summary>
        private static bool[,] grid = null;

        /// <summary>
        /// The second grid (used for preserving game state).
        /// </summary>
        private static bool[,] grid2 = null;

        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name='args'>
        /// The command-line arguments.
        /// </param>
        public static void Main(string[] args)
        {
            int steps = 0, gridSize = 0;
            Console.Write("The Game of Life.\n");

            // prompt for the number of steps
            Console.Write("How many steps in time? ");
            Console.ForegroundColor = ConsoleColor.Red;
            steps = Convert.ToInt32(Console.ReadLine());
            Console.ForegroundColor = ConsoleColor.White;

            // prompt for the size of the grid
            Console.Write("What size is the grid? ");
            Console.ForegroundColor = ConsoleColor.Red;
            gridSize = Convert.ToInt32(Console.ReadLine());
            Console.ForegroundColor = ConsoleColor.White;

            // prompt for the initial grid layout
            Console.Write("\nEnter the initial grid layout:\n");
            SetupGrid(gridSize, steps);
        }

        /// <summary>
        /// Prompts the user for the initial grid.
        /// </summary>
        /// <param name='size'>
        /// Size.
        /// </param>
        /// <param name='steps'>
        /// Steps.
        /// </param>
        public static void SetupGrid(int theSize, int steps)
        {
            string[] rows = new string[theSize];
            Console.ForegroundColor = ConsoleColor.Red;

            // accept input for the number of rows size allows
            for (int i = 0; i < theSize; i++)
            {
                rows [i] = Console.ReadLine();
            }

            Console.ForegroundColor = ConsoleColor.White;

            
            size = theSize;

            // parse the input
            grid = new bool[size, size];
            grid2 = new bool[size, size];
            ParseRows(rows);

            for (int j = 0; j < steps; j++)
            {
                Array.Copy(grid, grid2, grid.Length);
                PerformGameStep();
            }

            Console.Write("\nAfter {0} step(s):\n", steps);
            PrintAll();
        }

        /// <summary>
        /// Parses the rows from the setup grid.
        /// </summary>
        /// <param name='rows'>
        /// Rows.
        /// <param name='size'>
        /// Size.
        /// </param>
        public static void ParseRows(string[] rows)
        {
            // iterate over each string, for every row
            for (int i = 0; i < size; i++)
            {
                // for each column in a row
                for (int j = 0; j < size; j++)
                {
                    // see if alive or dead
                    if (rows [i] [j].CompareTo('O') == 0)
                    {
                        // set true if alive
                        grid [i, j] = true;
                    } else
                    {
                        // set false if dead
                        grid [i, j] = false;
                    }
                }
            }
        }

        /// <summary>
        /// Performs a step in the Game of Life.
        /// </summary>
        public static void PerformGameStep()
        {
            // let y represent the row
            // iterate over y
            for (int x = 0; x < size; x++)
            {
                // let x represent the column
                // iterate over x
                for (int y = 0; y < size; y++)
                {
                    SetShouldBeAlive(x, y);
                }
            }
        }

        /// <summary>
        /// Compares Neighbors to Determine if it should be Alive. Sets alive/dead.
        /// </summary>
        /// <returns><c>true</c>, if should be alive, <c>false</c> otherwise.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public static void SetShouldBeAlive(int x, int y)
        {
            int neighborsAlive = AliveNeighbors(x, y);

            // compare neighbors and set alive/dead
            if (neighborsAlive < 2)
            {
                grid [x, y] = false;
            }  else if (neighborsAlive == 3)
            {
                grid [x, y] = true;
            } else if (neighborsAlive > 3)
            {
                grid [x, y] = false;
            }
        }

        /// <summary>
        /// Finds the number of Alive neighbors.
        /// </summary>
        /// <returns>The number of alive neighbors.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public static int AliveNeighbors(int x, int y)
        {
            int neighborsAlive = 0;
            
            // only check upward neighbors if not at the edge...
            // check direct-left
            if (y != 0)
            {
                if (grid2 [x, y - 1])
                {
                    neighborsAlive++;
                }
            }
            // check up-left
            if (x != 0 && y != 0)
            {
                if (grid2 [x - 1, y - 1])
                {
                    neighborsAlive++;
                }
            }
            // check direct-up
            if (x != 0)
            {
                if (grid2 [x - 1, y])
                {
                    neighborsAlive++;
                }
            }
            // check up-right
            if (x != 0 && y != (size - 1))
            {
                if (grid2 [x - 1, y + 1])
                {
                    neighborsAlive++;
                }
            }
            // check direct-right
            if (y != (size - 1))
            {
                if (grid2 [x, y + 1])
                {
                    neighborsAlive++;
                }
            }
            // check down-right
            if (x != (size - 1) && y != (size - 1))
            {
                if (grid2 [x + 1, y + 1])
                {
                    neighborsAlive++;
                }
            }
            // check direct-down
            if (x != (size - 1))
            {
                if (grid2 [x + 1, y])
                {
                    neighborsAlive++;
                }
            }
            // check down-left
            if (x != (size - 1) && y != 0)
            {
                if (grid2 [x + 1, y - 1])
                {
                    neighborsAlive++;
                }
            }

            return neighborsAlive;
        }

        public static void PrintAll()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    // if it is alive
                    if (grid [i, j])
                    {
                        Console.Write("O");
                    }
                    // otherwise
                    else
                    {
                        Console.Write("-");
                    }
                }

                // add line break for next row
                Console.Write("\n");
            }
        }
    }
}
